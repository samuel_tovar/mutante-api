/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.test.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stovar
 */
@Entity
@Table(name = "mutante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mutante.findAll", query = "SELECT m FROM Mutante m"),
    @NamedQuery(name = "Mutante.findById", query = "SELECT m FROM Mutante m WHERE m.id = :id"),
    @NamedQuery(name = "Mutante.findByRespuesta", query = "SELECT m FROM Mutante m WHERE m.respuesta = :respuesta")})
public class Mutante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Column(name = "respuesta")
    private Integer respuesta;
    @Lob
    @Size(max = 65535)
    @Column(name = "adn")
    private String adn;

    public Mutante() {
    }

    public Mutante(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Integer respuesta) {
        this.respuesta = respuesta;
    }

    public String getAdn() {
        
        return adn;
    }

    public void setAdn(String adn) {
        this.adn = adn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mutante)) {
            return false;
        }
        Mutante other = (Mutante) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.samuel.test.Mutante[ id=" + id + " ]";
    }
    
}
