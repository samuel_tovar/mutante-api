/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.test.recursos;

import com.samuel.test.modelo.Mutante;
import com.samuel.test.service.MutanteService;
import javax.ws.rs.core.Response;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author stovar
 */
@Stateless
@Path("/mutant")
public class MutanteFacadeREST  {

   @EJB
   MutanteService mutante;
   @Context  //injected response proxy supporting multiple threads
   private HttpServletResponse response;

    public MutanteFacadeREST() {
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response crear(Mutante entities){
      try{
        mutante.create(entities);
        if(entities.getRespuesta()>1){
            return Response.status(Response.Status.OK.getStatusCode()).entity(entities).type(MediaType.APPLICATION_JSON).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).entity(entities).type(MediaType.APPLICATION_JSON).build();
        }
        //response.setStatus(Response.Status.OK.getStatusCode());
      }catch(Exception ex){
          return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).entity(ex.getMessage()).type(MediaType.APPLICATION_JSON).build();
          //response.setStatus(Response.Status..getStatusCode());
      }
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void editar(@PathParam("id") Integer id, Mutante entity) {
        mutante.edit(id,entity);
    }

   /* @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }*/

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Mutante buscar(@PathParam("id")Integer id) {
        return mutante.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Mutante> mostrarAll() {
        return mutante.findAll();
    }


    @GET
    @Path("/stats")
    @Produces(MediaType.TEXT_PLAIN)
    public String Esatdistica() {
        return String.valueOf(mutante.countREST());
    }

}
