/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.test.recursos;

import com.samuel.test.modelo.Mutante;
import com.samuel.test.service.MutanteService;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author stovar
 */
@Stateless
@Path("/stats")
public class StatsFacadeREST {
    @Context
    private UriInfo context;
    @EJB
   MutanteService mutante;
    
    public StatsFacadeREST() {
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public String Esatdistica() {
        int conth=0; int contm=0;
        String res="";
        float result=0;
        List<Mutante> lm =mutante.findAll();
        for (Iterator<Mutante> it = lm.iterator(); it.hasNext();) {
            Mutante lis = it.next();
           if(lis.getRespuesta()>1){
               contm++;
           }else{
               conth++;
           }
           
        }
        result = contm/conth;
        res="{\"count_mutant_dna\":\""+contm+"\",\"count_human_dna\":\""+conth+"\",\"ratio\":\""+result+"\"}";
        //return String.valueOf(mutante.countREST());
        return res;
    }
    
}
