/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.test.service;

import com.samuel.test.dao.AbstractFacade;
import com.samuel.test.modelo.Adn;
import com.samuel.test.modelo.Mutante;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author stovar
 */
@Stateless
public class MutanteService extends AbstractFacade<Mutante>{
    @PersistenceContext(unitName = "restMutantePU")
    private EntityManager em;
    @EJB
    Adn dna;

    public MutanteService() {
        super(Mutante.class);
    }
    @Override
    public void create(Mutante entity) {
        System.out.println(entity.getAdn());
        dna.explode(entity.getAdn());
        entity.setRespuesta(dna.isMutant());
        super.create(entity);
    }
    
    public void edit(Integer id, Mutante entity) {
        super.edit(entity);
    }
    
    public Mutante find(Integer id) {
        return super.find(id);
    }
    
    @Override
    public List<Mutante> findAll() {
        return super.findAll();
    }
    
    @Override
    public List<Mutante> findWithQuery(String sql){
        return super.findWithQuery(sql);
    }
    
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
